package com.wortuallabs.logger;

/**
 * Created by neeraj on 11/9/15.
 */
public class AndroidLogger {
    private static AndroidLogger ourInstance = new AndroidLogger();

    public static AndroidLogger getInstance() {
        return ourInstance;
    }

    public static String getVersion() {
        return "Android Logger 1.0" ;
    }

    private AndroidLogger() {
    }
}
